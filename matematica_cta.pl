#!/usr/bin/perl o
#!/usr/bin/env perl

#-------------------CREDITS-------------------#

#PROGRAMA: matematica.pl                      #
#CREATED by MR_ROBOT(KEVIN HUANCA FERNANDEZ)  #
#GROUP by FSOCIETY                            #
#CODED by MR_ROBOT(KEVIN HUANCA FERNANDEZ)    #
#Version: 5.0.0.1                             #

#-------------------CREDITS-------------------#

#COLORES:

$v="\033[1;31m";     #rojo normal
$vs="\033[04;31m";   #rojo subliminado
$v0="\033[0;31m";    #rojo mas oscuro
$v7="\033[07;31m";   #rojo reverse
$v8="\033[08;31m";   #rojo oculto

$V="\033[1;32m";     #Verde normal
$Vs="\033[04;32m";   #Verde subliminado
$V0="\033[0;32m";    #Verde mas oscuro
$V7="\033[07;32m";   #Verde reverse
$V8="\033[08;32m";   #Verde oculto

$r="\033[1;35m";     #Rosado normal
$rs="\033[04;35m";   #Rosado subliminado
$r0="\033[0;35m";    #Rosado mas oscuro
$r7="\033[07;35m";   #Rosado reverse
$r8="\033[08;35m";   #Rosado oscuro

$c="\033[1;36m";     #cyan normal
$cs="\033[04;36m";   #cyan subliminado
$c0="\033[0;36m";    #cyan mas oscuro
$c7="\033[07;36m";   #cyan reverse
$c8="\033[08;36m";   #cyan oculto

$a="\033[1;34m";     #Azul normal
$as="\033[04;34m";   #Azul subliminado
$a0="\033[0;34m";    #Azul mas oscuro
$a7="\033[07;34m";   #Azul reverse
$a8="\033[08;34m";   #Azul oculto

$A="\033[1;33m";     #Amarillo normal
$As="\033[04;33m";   #Amarillo subliminado
$A0="\033[0;33m";    #Amarillo mas oscuro
$A7="\033[07;33m";   #Amarillo reverse
$A8="\033[08;33m";   #Amarillo oculto

$b="\033[1;37m";     #Blanco normal
$bs="\033[04;37m";   #Blanco subliminado
$b0="\033[0;37m";    #Blanco mas oscuro
$b7="\033[07;37m";   #Blanco reverse
$b8="\033[08;37m";   #Blanco oculto

$g="\033[1;30m";     #Gris normal
$gs="\033[04;30m";   #Gris subliminado
$g0="\033[0;30m";    #Gris mas oscuro
$g7="\033[07;30m";   #Gris reverse
$g8="\033[08;30m";   #Gris oculto

$end="\033[1;m";

$suma="+";
$resta="-";
$multiplica="*";
$division="/";

import os;

#STARTING..

print "$c  _______  ______  _______  _______  _______  _______  _______  _______  ______  _______$end \n";
print "$c |   _   ||   __ \ |_     _||_    _| |   |   ||    ___||_     _||_     _||      ||   _   |$end\n";
print "$c |       ||      < _|   |_   |   |  |       ||    ___|  |   |   _|   |_ |   ---||       |$end\n";
print "$c |___|___||___|__||_______|  |___|  |__|_|__||_______|  |___|  |_______||______||___|___|$end\n";
print "$V                                    by$v KevinHF                                \n";
print "$a MENU: \n";
print "         \n";
print "$V   [$b 01$V ]$b Suma$end \n";
print "$V   [$b 02$V ]$b Resta$end \n";
print "$V   [$b 03$V ]$b Dividir$end \n";
print "$V   [$b 04$V ]$b Multiplicacion$end \n";
print "$V   [$b 05$V ]$b Sumar un numero par desde el 1 asta su mismo numero$end \n";
print "$V   [$b 06$V ]$b Porcentaje$end \n";
print "$V   [$b 07$V ]$b Calcular El Gasto Energetico en reposo (CTA)$end \n";
print "$V   [$b 08$V ]$b Calcular El Gasto Energetico Total y Requerimiento energetico diario (CTA)$end \n";
print "$V   [$b 09$V ]$b Calcular Glucidos, Lipidos, Proteinas en kcal/g (CTA) $end \n";
print "$V   [$b 10$V ]$b Calcular Energia Global, Energia Digestiva, Energia Metabolica kcal (CTA)$end \n";
print "$V   [$b 11$V ]$b Calcular el porcentaje de mi RED que cubre si consumo un alimento (CTA)$end \n";
print "$V   [$b 12$V ]$b Calcular cuantas kcal hay en tantos gramos de un alimento(CTA)$end \n";
print "$V   [$b 13$V ]$b Calcular IMC(Indice de Masa corporal) (CTA) $end\n";
print "$V   [$b 99$V ]$b Exit$end \n";
print "                          \n";
print ("ARITMETICA: ");
$op_menu=<STDIN>;
os.system(sleep 0.5);

open(DATAFILE, '>>opc.conf');
 print DATAFILE $op_menu;
close(DATAFILE);

if ($op_menu > 0){
if ($op_menu > 13){
if ($op_menu != 99){
chop($op_menu);
print "$v $end \n";
print "[-] La Opcion $op_menu no esta disponible o no existe ...\n";
os.system(sleep 0.9);
exit;
}
}
}

if ($op_menu == 1 ) {

		print "$A Digame el primer numero que quiere sumar$b:$b $end";
			$numero1=<STDIN>;

		print "$A Digame el segundo numero que quiere sumar$b:$b $end ";
			$numero2=<STDIN>;

				chop($numero1);
				chop($numero2);

		print "[*] Sumando$v ( $numero1 + $numero2 )$b .....$end \n";

					$respuesta=$numero1 + $numero2;

		print "[+] Respuesta =$V $respuesta $end \n";

}

if ($op_menu == 2 ) {

		print "$A Digame el primer numero que quiere restar$b:$b $end ";
			$numeroresta1=<STDIN>;

		print "$A Digame el segundo numero que quiere restar$b:$b $end ";
			$numeroresta2=<STDIN>;

				chop($numeroresta1);
				chop($numeroresta2);

		print "[*] Restando$v ( $numeroresta1 - $numeroresta2 )$b .... $end \n";

					$respuesta1=$numeroresta1 - $numeroresta2;
		print "[+] Respuesta =$V $respuesta1 $end \n";

}

if ($op_menu == 3 ) {

		print "$A Digame el primer numero que quiere dividir$b:$b $end ";
			$numerodividir1=<STDIN>;

		print "$A Digame el segundo numero que quiere dividir$b:$b $end ";
			$numerodividir2=<STDIN>;
			
				chop($numerodividir1);
			chop($numerodividir2);

		print "[*] Dividiendo$v ($numerodividir1 $division $numerodividir2 )$b ... $end \n";

					$respuestadividir1=$numerodividir1 / $numerodividir2;

		print "[+] Respuesta =$V $respuestadividir1 $end \n";

}

if ($op_menu == 4 ) {

		print "$A Digame el primer numero que quiere multiplicar$b:$b $end";
			$numeromultipli1=<STDIN>;

		print "$A Digame el segundo numero que quiere multiplicar$b:$b $end";
			$numeromultipli2=<STDIN>;

				chop($numeromultipli1);
				chop($numeromultipli2);

		print "[*] Multiplicando$v ( $numeromultipli1 x $numeromultipli2 )$b ... $end \n";

					$respuestamultipli1=$numeromultipli1 * $numeromultipli2;

		print "[+] Respuesta =$V $respuestamultipli1 $b $end \n";
}

if ($op_menu == 5 ){
print "$A $end \n";
		print("Digame un numero que sea par: ");
		$numberpar=<STDIN>;
		chop($numberpar);
		print "$b $end \n";
		print "[*] Sumando o Usando Formula ...\n";
		os.system(sleep 0.7);
		$ejec=$numberpar + 1;
		$ejec2=$numberpar / 2;
		$ejec3=$ejec * $ejec2;
		print "[+] La suma del 1 al $numberpar es$V $ejec3 $b $end ...\n";
		exit;
}

if ($op_menu == 6 ){
		print "$A $end \n";
		print("Digame el porcentaje: ");
		$PORCENTAJE06=<STDIN>;
		chop($PORCENTAJE06);
		print("Digame el total: ");
		$TOTALPORCENTAJE06=<STDIN>;
		print "$b $end \n";
		print "[*] Calculando Le porcentaje ...\n";
		$RESPUESTAPORCENTAJE06=$PORCENTAJE06 * $TOTALPORCENTAJE06;
		$RESPUESTAPORCENTAJE06=$RESPUESTAPORCENTAJE06 / 100;
		$RESPUESTATRUE06=$RESPUESTAPORCENTAJE06;
		os.system(sleep 0.7);
		print "[+] El porcentaje de $PORCENTAJE06 es$V $RESPUESTATRUE06 $b $end ...\n";
		exit;

}

if ($op_menu == 99 ){
print "$v $end\n";
		print "[*] Exiting ... $end \n";
			os.system(sleep 1);
		print "$b $end \n";
				exit;

}

if ($op_menu == 7 ){

&GERCTA( );
	sub GERCTA{
		local($GERPEOPLE,$A,$PA,$E,$PROCESO,$RESULTADO,$PROCESO2,$RESULTADO2);
		print "$c $end \n";
		print "[?] OPCIONES: \n";
		print "1)--> HOMBRE \n";
		print "2)--> MUJER$a $end \n";
		print "Digame de que Genero quiere resolver el problema:$b$a $end";
		$GERPEOPLE=<STDIN>;
			if ($GERPEOPLE == 1 ){
			print("Digame su Altura de la persona: $end ");
			$A=<STDIN>;
			print("Digame su Peso Actual de la persona:$a $end ");
			$PA=<STDIN>;
			print("Digame su Edad de la persona:$a $end ");
			$E=<STDIN>;
			print "$b $end \n";
			print "[*] Resolviendo Su GER (Gasto Energetico en Reposo) del Hombre ... \n";  
			os.system(sleep 0.5);
			$PROCESO=66 + (13.7 * $PA) + (5 * $A) - (6.8 * $E);
			$RESULTADO=$PROCESO;
			$PROCESO2=$RESULTADO / 24;
			$RESULTADO2=$PROCESO2;
			print "[+] Su Resultado es$V [$RESULTADO kcal/dia]$b y$V [$RESULTADO2 kcal/hora]$b$end ... \n";
			exit;
			}
			if ($GERPEOPLE == 2 ){
print("Digame su Altura de la persona: $end ");
			$A=<STDIN>;
			print("Digame su Peso Actual de la persona:$a $end ");
			$PA=<STDIN>;
			print("Digame su Edad de la persona:$a $end ");
			$E=<STDIN>;
			print "$b $end \n";
			print "[*] Resolviendo Su GER (Gasto Energetico en Reposo) de la mujer ... \n";  
			os.system(sleep 0.5);
			$PROCESO=655.1 + (9.56 * $PA) + (1.85 * $A) - (4.68 * $E);
			$RESULTADO=$PROCESO;
			$PROCESO2=$RESULTADO / 24;
			$RESULTADO2=$PROCESO2;
			print "[+] El Resultado es$V [$RESULTADO kcal/dia]$b y$V [$RESULTADO2 kcal/hora]$b$end ... \n";
			exit;
			}
	}
}

if ($op_menu == 8 ){

	&GETCTA( );

		sub GETCTA{
		
local($dataact,$datahour,$e,$GERHORA,$lista,$H0RA,$numberactividades,$numberactividades2,$i,$i2,@actividades,@actividades2,$RESPUESTA08);
		print "$r $end \n";
		print("Digame su GER kcal/hora: ");
		$GERHORA=<STDIN>;
		print "$c $end \n";
		print "[*] Lista de Actividades:\n";
		print "1 --> Muy Ligera: reposo,dormir,estirarse = 1.3 \n";
		print "2 --> Ligeras: Actividades que hacemos sentados o de pie como escribir,leer,cocinar,conducir = 1.5 \n";
		print "3 --> Moderado: pasear,cuidar niños,andar(no muy rapido 4 - 5 km/h) = 1.6 \n";
		print "4 --> Intenso: ir en bicicleta,llevar peso,andar(a ritmo 5.5 - 6.5 km/h),jugar tennis = 1.9 \n";
		print "5 --> Muy Intenso: jugar futbol,basket,transportar paquetes muy pesados = 2.2 \n";
		print "6 --> Infeccion Level = 1.2 \n";
		print "7 --> Infeccion Moderada = 1.4\n";
		print "8 --> Infeccion Grave = 1.6 \n";
		print "9 --> Cirugia Menor = 1.1 \n";
		print "10 --> Cirugia Grave = 1.2 \n";
		print "11 --> Traumatismo del esqueleto = 1.5\n";
		print "12 --> Traumatismo craneal = 1.6 $A $end\n";
		print("Digame Cuantas Actividades hace al dia: ");
		$numberactividades=<STDIN>;
		$numberactividades2=$numberactividades * 2;
		$e=1;
		until ($e > $numberactividades2){
		print("Digame Su Factor establecido de la actividad: ");
		$dataact=<STDIN>;
		@actividades[$i]=$dataact;
		$i=$i + 1;
		print("Digame las horas que realiza esa actividad: ");
		$datahour=<STDIN>;
		@actividades[$i]=$datahour;
		$i=$i + 1;
		$e=$e + 2;
		}
		$i=0;
		$i2=0;
		$i3=1;
		printf("%s %s \n",$b,$end);
		print "[*] Calculando Operaciones ...\n";
		until ($i > $numberactividades){
		@actividades2[$i]=$GERHORA * @actividades[$i2] * @actividades[$i3];
		print "[+] El Gasto Energetico Total $i actividad es$V @actividades2[$i]$b $end ... \n";
		if ($i <= $numberactividades){
		$RESPUESTA08=$RESPUESTA08 + @actividades2[$i];
		}
		$i2=$i2 + 2;
		$i3=$i3 + 2;
		$i=$i + 1;
		}
		print "[*] Operaciones matematicas resueltas !! ... \n";
		os.system(sleep 0.8);
		print "[*] Calculando el Valor Total de RED ... \n";
		os.system(sleep 0.5);
		print "[+] El Resultado de RED(Requerimiento Energetico Diario) es$V $RESPUESTA08 kcal/dia$b$end ...\n";
		exit;
	}
}

if ($op_menu == 9 ){

	&CTAGLP( );
		sub CTAGLP{
		print "$g $end \n";
		print "[?] OPCIONES: \n";
		print "1 --> Glucidos \n";
		print "2 --> Lipidos \n";
		print "3 --> Proteinas \n";
		print "4 --> Todos$c $end \n";
		print("Digame la opcion: ");
		$opcionGLP=<STDIN>;
		if ($opcionGLP == 1 ){
		print("Digame el Contenido en g: ");
		$contenido1=<STDIN>;
		chop($contenido1);
		$PROCESO=$contenido1 * 4;
		$RESPUESTA=$PROCESO;
		print "$b $end \n";
		print "[*] Calculando Sus kcal de $contenido1 g de Glucidos ...\n";
		os.system(sleep 0.8);
		print "[+] El resultado es$V $RESPUESTA kcal$b de $contenido1 g en Glucidos$end \n";
		exit;
		}
		if ($opcionGLP == 2 ){
                print("Digame el Contenido en g: ");
                $contenido1=<STDIN>;
		chop($contenido1);
                $PROCESO=$contenido1 * 9;
                $RESPUESTA=$PROCESO;
                print "$b $end \n";
                print "[*] Calculando Sus kcal de $contenido1 g de Lipidos ...\n";
                os.system(sleep 0.8);
                print "[+] El resultado es$V $RESPUESTA kcal$b de $contenido1 g en Lipidos$end \n";
                exit;
                }
		if ($opcionGLP == 3 ){
                print("Digame el Contenido en g: ");
                $contenido1=<STDIN>;
		chop($contenido1);
                $PROCESO=$contenido1 * 4;
                $RESPUESTA=$PROCESO;
                print "$b $end \n";
                print "[*] Calculando Sus kcal de $contenido1 g de Proteinas ...\n";
                os.system(sleep 0.8);
                print "[+] El resultado es$V $RESPUESTA kcal$b de $contenido1 g en Proteinas$end \n";
                exit;
                }

		if ($opcionGLP == 4 ){
                print("Digame el Contenido en g de Glucidos: ");
                $contenido1=<STDIN>;
		print("Digame el Contenido en g de Lipidos: ");
		$contenido2=<STDIN>;
		print("Digame el Contenido en g de Proteinas: ");
		$contenido3=<STDIN>;
                $PROCESO1=$contenido1 * 4;
		$PROCESO2=$contenido2 * 9;
		$PROCESO3=$contenido3 * 4;
		$PROCESO4=$PROCESO1 + $PROCESO2 + $PROCESO3;
                $RESPUESTA=$PROCESO1;
                print "$b $end \n";
                print "[*] Calculando Sus kcal de g de GLP ...\n";
                os.system(sleep 0.8);
                print "[+] El resultado es$V [G: $RESPUESTA kcal] [L: $PROCESO2 kcal] [P: $PROCESO3 kcal] $b de  g en GLP$end \n";
                print "[+] La suma de los Glucidos,Lipidos y Proteinas es de$V $PROCESO4 kcal$b en total ...$end \n";
                exit;
                }

	}

}

if ($op_menu == 10 ){
                print "$g $end \n";
                print "OPTIONS :
1) --> Usar El Metodo Normal (Normal)
2) --> Usar El Metodo del cangrejo (de reversa)$A $end \n";
                print "Eliga Una Opcion: ";
                $OP_MINU=<STDIN>;
                if($OP_MINU == 1){
				print "$r $end \n";
				print("Digame la cantidad de Glucidos en kcal: ");
				$CG=<STDIN>;
				print("Digame la cantidad de Lipidos en kcal: ");
				$CL=<STDIN>;
				print("Digame la cantidad de Proteinas en kcal: ");
				$CP=<STDIN>;
				print("Digame la cantidad de Energia Fecal en kcal: ");
				$EF=<STDIN>;
				print("Digame la cantidad de Energia Orinaria en kcal: ");
				$EO=<STDIN>;
				print "$b $end \n";
				print "[*] Desarrollando problemas ...\n";
				$EM=$CG + $CL + $CP;
				$ED=$EM + $EO;
				$EG=$ED + $EF;
				os.system(sleep 0.8);
				print "[+] La Energia Global(EG) es$V $EG $b $end ...\n";
				print "[+] La Energia Digestible(ED) es$V $ED $b $end ...\n";
				print "[+] La Energia Metabolizada(EM) es$V $EM $b $end ...\n";
				exit;
				}
				if($OP_MINU == 2){
				print "$r $end \n";
				print "Digame la EM(Energia Metabolizable): ";
				$EM=<STDIN>;
				chop($EM);
				print "Digame la Energia Orinaria: ";
				$EO=<STDIN>;
				print "Digame la Energia Fecal: ";
				$EF=<STDIN>;
				print "$b $end \n";
				print "[*] Calculando la ED,EG,EM ...\n";
				os.system(sleep 0.8);
				$ED=$EM + $EO;
				$EG=$ED + $EF;
				print "[+] La Respuesta es:$V [EG: $EG kcal] [ED: $ED kcal] [EM: $EM kcal]$b ...$end\n";
				exit;
				}
}

if ($op_menu == 11){
print "$r $end \n";
print("Digame su RED(Requerimiento Energetico Diario): ");
$RED=<STDIN>;
print("Digame el total de kcal que aporta el alimento: ");
$ALIMENTOKCAL=<STDIN>;
os.system(sleep 0.8);
print "$b $ene \n";
print "[*] Calculando Porcentaje que cubre el alimento ...\n";
$PROCESO11=$ALIMENTOKCAL * 100;
$PROCESO11=$PROCESO11 / $RED;
$RESULTADO11=$PROCESO11;
$RESULTADOREDONDEO=sprintf("%.2f", $RESULTADO11);
print "[+] El Porcentaje que cubre el alimento de mi RED es de$V $RESULTADOREDONDEO % $b ...$end \n";
exit;
}


if($op_menu == 12){

printf(" %s %s \n",$A,$end);
print "Cuantas Kcal Hay en 100g del alimento: ";
$CANTIDAD=<STDIN>;
print "De cuantos gramos quiere sacar: ";
$CANTIDADPRD=<STDIN>;
chop($CANTIDADPRD);

print "$b $end \n";
print "[*] Calculando Cuantas Kcal hay en$V $CANTIDADPRD g$b del alimento ...$end\n";
os.system(sleep 1);
$PROCESO121=$CANTIDAD * $CANTIDADPRD;
$PROCESO122=$PROCESO121 / 100;
$RESPUESTA112=$PROCESO122;
print "[*] Operaciones Calculadas ... !!!\n";
print "[+] Las Kcal que aporta de 100g en $CANTIDADPRD g es de$V $RESPUESTA112 kcal$b ...$end \n";
exit;
}

if ($op_menu == 13){
print "$c $end\n";
print("Digame su Peso en Kg: ");
$PESOKG=<STDIN>;
print("Digame su Altura en metros: ");
$ALTURA=<STDIN>;
print "$b $end \n";
print "[*] Calculando Su Indice de masa corporal ...\n";
$ALTURA2=$ALTURA * $ALTURA;
$PROCESO12=$PESOKG / $ALTURA2;
$RESPUESTA12=$PROCESO12;
os.system(sleep 0.8);
if ($RESPUESTA12 < 18.49){
$ESTADOPEOPLE12="Infrapeso";
print "[+] Su IMC es$V $RESPUESTA12 $b...$end\n";
print "[+] Su Estado es:$v $ESTADOPEOPLE12 $b $end \n";
exit;
}
if ($RESPUESTA12 >= 40){
$ESTADOPEOPLE12="Obesidad morbida";
print "[+] Su IMC es:$V $RESPUESTA12 $b...$end\n";
print "[+] Su Estado es:$v $ESTADOPEOPLE12 $b $end \n";
exit;
}
if ($RESPUESTA12 > 18.50){
if ($RESPUESTA12 <= 24.99){
$ESTADOPEOPLE12="Peso Normal";
print "[+] Su IMC es:$V $RESPUESTA12 $b...$end\n";
print "[+] Su Estado es:$V $ESTADOPEOPLE12 $b $end \n";
exit;
}else{
}
}

if ($RESPUESTA12 > 25){
if ($RESPUESTA12 <= 29.99){
$ESTADOPEOPLE12="Sobrepeso";
print "[+] Su IMC es:$V $RESPUESTA12 $b...$end\n";
print "[+] Su Estado es:$v $ESTADOPEOPLE12 $b $end \n";
exit;
}else{
}
}

if ($RESPUESTA12 > 30){
if ($RESPUESTA12 <= 34.99){
$ESTADOPEOPLE12="Obesidad leve";
print "[+] Su IMC es:$V $RESPUESTA12 $b...$end\n";
print "[+] Su Estado es:$v $ESTADOPEOPLE12 $b $end \n";
exit;
}else{
}
}

if ($RESPUESTA12 > 35){
if ($RESPUESTA12 <= 39.99){
$ESTADOPEOPLE12="Obesidad media";
print "[+] Su IMC es:$V $RESPUESTA12 $b...$end\n";
print "[+] Su Estado es:$v $ESTADOPEOPLE12 $b $end \n";
exit;
}else{
}
}
}



