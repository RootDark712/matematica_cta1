#!/usr/bin/perl o
#!/usr/bin/env perl

#-------------------CREDITS-------------------#

#PROGRAMA: matematica.pl                      #
#CREATED by MR_ROBOT(KEVIN HUANCA FERNANDEZ)  #
#GROUP by FSOCIETY                            #
#CODED by MR_ROBOT(KEVIN HUANCA FERNANDEZ)    #
#Version: 5.0.0.1                             #

#-------------------CREDITS-------------------#

#COLORES:

$v="\033[1;31m";     #rojo normal
$vs="\033[04;31m";   #rojo subliminado
$v0="\033[0;31m";    #rojo mas oscuro
$v7="\033[07;31m";   #rojo reverse
$v8="\033[08;31m";   #rojo oculto

$V="\033[1;32m";     #Verde normal
$Vs="\033[04;32m";   #Verde subliminado
$V0="\033[0;32m";    #Verde mas oscuro
$V7="\033[07;32m";   #Verde reverse
$V8="\033[08;32m";   #Verde oculto

$r="\033[1;35m";     #Rosado normal
$rs="\033[04;35m";   #Rosado subliminado
$r0="\033[0;35m";    #Rosado mas oscuro
$r7="\033[07;35m";   #Rosado reverse
$r8="\033[08;35m";   #Rosado oscuro

$c="\033[1;36m";     #cyan normal
$cs="\033[04;36m";   #cyan subliminado
$c0="\033[0;36m";    #cyan mas oscuro
$c7="\033[07;36m";   #cyan reverse
$c8="\033[08;36m";   #cyan oculto

$a="\033[1;34m";     #Azul normal
$as="\033[04;34m";   #Azul subliminado
$a0="\033[0;34m";    #Azul mas oscuro
$a7="\033[07;34m";   #Azul reverse
$a8="\033[08;34m";   #Azul oculto

$A="\033[1;33m";     #Amarillo normal
$As="\033[04;33m";   #Amarillo subliminado
$A0="\033[0;33m";    #Amarillo mas oscuro
$A7="\033[07;33m";   #Amarillo reverse
$A8="\033[08;33m";   #Amarillo oculto

$b="\033[1;37m";     #Blanco normal
$bs="\033[04;37m";   #Blanco subliminado
$b0="\033[0;37m";    #Blanco mas oscuro
$b7="\033[07;37m";   #Blanco reverse
$b8="\033[08;37m";   #Blanco oculto

$g="\033[1;30m";     #Gris normal
$gs="\033[04;30m";   #Gris subliminado
$g0="\033[0;30m";    #Gris mas oscuro
$g7="\033[07;30m";   #Gris reverse
$g8="\033[08;30m";   #Gris oculto

$end="\033[1;m";

$suma="+";
$resta="-";
$multiplica="*";
$division="/";

import os;
use Math::Complex;
use Time::HiRes;

#STARTING..

print "$c  _______  ______  _______  _______  _______  _______  _______  _______  ______  _______$end \n";
print "$c |   _   ||   __ \ |_     _||_    _| |   |   ||    ___||_     _||_     _||      ||   _   |$end\n";
print "$c |       ||      < _|   |_   |   |  |       ||    ___|  |   |   _|   |_ |   ---||       |$end\n";
print "$c |___|___||___|__||_______|  |___|  |__|_|__||_______|  |___|  |_______||______||___|___|$end\n";
print "$V                                    by$v KevinHF                                \n";
print "$a MENU: \n";
print "         \n";
print "$V   [$b 01$V ]$b Suma$end \n";
print "$V   [$b 02$V ]$b Resta$end \n";
print "$V   [$b 03$V ]$b Dividir$end \n";
print "$V   [$b 04$V ]$b Multiplicacion$end \n";
print "$V   [$b 05$V ]$b Sumar un numero par desde el 1 asta su mismo numero$end \n";
print "$V   [$b 06$V ]$b Porcentaje$end \n";
print "$V   [$b 07$V ]$b Raiz Cuadrada$end \n";
print "$V   [$b 08$V ]$b Perimetros$end \n";
print "$V   [$b 09$V ]$b Areas $end \n";
print "$V   [$b 10$V ]$b Pitagoras$end \n";
print "$V   [$b 99$V ]$b Exit$end \n";
print "                          \n";
print ("ARITMETICA: ");
$op_menu=<STDIN>;
os.system(sleep 0.5);

open(DATAFILE, '>>opc.conf');
 print DATAFILE $op_menu;
close(DATAFILE);

if ($op_menu > 0){
if ($op_menu > 10){
if ($op_menu != 99){
chop($op_menu);
print "$v $end \n";
print "[-] La Opcion $op_menu no esta disponible o no existe ...\n";
os.system(sleep 0.9);
exit;
}
}
}

if ($op_menu == 1 ) {

		print "$A Digame el primer numero que quiere sumar$b:$b $end";
			$numero1=<STDIN>;

		print "$A Digame el segundo numero que quiere sumar$b:$b $end ";
			$numero2=<STDIN>;

				chop($numero1);
				chop($numero2);

		print "[*] Sumando$v ( $numero1 + $numero2 )$b .....$end \n";

					$respuesta=$numero1 + $numero2;

		print "[+] Respuesta =$V $respuesta $end \n";

}

if ($op_menu == 2 ) {

		print "$A Digame el primer numero que quiere restar$b:$b $end ";
			$numeroresta1=<STDIN>;

		print "$A Digame el segundo numero que quiere restar$b:$b $end ";
			$numeroresta2=<STDIN>;

				chop($numeroresta1);
				chop($numeroresta2);

		print "[*] Restando$v ( $numeroresta1 - $numeroresta2 )$b .... $end \n";

					$respuesta1=$numeroresta1 - $numeroresta2;
		print "[+] Respuesta =$V $respuesta1 $end \n";

}

if ($op_menu == 3 ) {

		print "$A Digame el primer numero que quiere dividir$b:$b $end ";
			$numerodividir1=<STDIN>;

		print "$A Digame el segundo numero que quiere dividir$b:$b $end ";
			$numerodividir2=<STDIN>;
			
				chop($numerodividir1);
			chop($numerodividir2);

		print "[*] Dividiendo$v ($numerodividir1 $division $numerodividir2 )$b ... $end \n";

					$respuestadividir1=$numerodividir1 / $numerodividir2;

		print "[+] Respuesta =$V $respuestadividir1 $end \n";

}

if ($op_menu == 4 ) {

		print "$A Digame el primer numero que quiere multiplicar$b:$b $end";
			$numeromultipli1=<STDIN>;

		print "$A Digame el segundo numero que quiere multiplicar$b:$b $end";
			$numeromultipli2=<STDIN>;

				chop($numeromultipli1);
				chop($numeromultipli2);

		print "[*] Multiplicando$v ( $numeromultipli1 x $numeromultipli2 )$b ... $end \n";

					$respuestamultipli1=$numeromultipli1 * $numeromultipli2;

		print "[+] Respuesta =$V $respuestamultipli1 $b $end \n";
}

if ($op_menu == 5 ){
print "$A $end \n";
		print("Digame un numero que sea par: ");
		$numberpar=<STDIN>;
		chop($numberpar);
		print "$b $end \n";
		print "[*] Sumando o Usando Formula ...\n";
		os.system(sleep 0.7);
		$ejec=$numberpar + 1;
		$ejec2=$numberpar / 2;
		$ejec3=$ejec * $ejec2;
		print "[+] La suma del 1 al $numberpar es$V $ejec3 $b $end ...\n";
		exit;
}

if ($op_menu == 6 ){
		print "$A $end \n";
		print("Digame el porcentaje: ");
		$PORCENTAJE06=<STDIN>;
		chop($PORCENTAJE06);
		print("Digame el total: ");
		$TOTALPORCENTAJE06=<STDIN>;
		print "$b $end \n";
		print "[*] Calculando Le porcentaje ...\n";
		$RESPUESTAPORCENTAJE06=$PORCENTAJE06 * $TOTALPORCENTAJE06;
		$RESPUESTAPORCENTAJE06=$RESPUESTAPORCENTAJE06 / 100;
		$RESPUESTATRUE06=$RESPUESTAPORCENTAJE06;
		os.system(sleep 0.7);
		print "[+] El porcentaje de $PORCENTAJE06 es$V $RESPUESTATRUE06 $b $end ...\n";
		exit;

}

if ($op_menu == 99 ){
print "$v $end\n";
		print "[*] Exiting ... $end \n";
			os.system(sleep 1);
		print "$b $end \n";
				exit;

}
if($op_menu == 7){
print "$v $end \n";
print "De que numero desea sacar raiz cuadrada: ";
$NUMBERRAIZ=<STDIN>;
chop($NUMBERRAIZ);
print "$b $end \n";
print "[*] Calculando La Raiz cuadrada de$V $NUMBERRAIZ $b ...$end \n";
os.system(sleep 0.8);
$RESPUESTA7=sqrt($NUMBERRAIZ);
print "[+] La Raiz Cuadrada de $NUMBERRAIZ es$V $RESPUESTA7 $b ...$end \n";
exit;
}

if($op_menu == 8){
print "$g $end \n";
print "FIGURAS PLANAS:
1 --> Cuadrado
2 --> Rectangulo
3 --> Triangulo Equilatero \n";
print "$r $end \n";
print "Digame que figura desea sacar su perimetro: ";
$OP_FIGURAS=<STDIN>;
if($OP_FIGURAS == 1){              #OPCION 1;
print "$a \n";
print "Digame El Lado del Cuadrado(X): ";
$ladocuadrado=<STDIN>;
print "Digame su Perimetro del cuadrado: ";
$perimetrodelcuadrado=<STDIN>;
print "$b \n";
if ($ladocuadrado == "X"){
print "[*] Calculando su lado del cuadrado ...\n";
system(sleep 0.8);
$result=$perimetrodelcuadrado / 4;
print "[+] El lado del cuadrado es$V $result $b...$end\n";
exit;
}
if($perimetrodelcuadrado == "X"){
print "[*] Calculando el perimetro del Cuadrado... \n";
system(sleep 0.7);
$rsult=$ladocuadrado * 4;
print "[+] El perimetro del cuadrado es$V $rsult $b...\n";
exit;
}
}    #FALTA TERMINAR;
if($OP_FIGURAS == 2){
print "$A \n";
print "Digame el largo del rectangulo(X): ";
$largo1=<STDIN>;
print "Digame el ancho del rectangulo(X): ";
$ancho1=<STDIN>;
print "Digame el perimetro del rectangulo(X): ";
$perimetr0=<STDIN>;
print "$b \n";
if($largo1 == "X"){          #FORMULA: P=2(BMA) + 2(BME);
print "[*] Calculando el largo del rectangulo ...\n";
$result0=$ancho1 * 2;
$result1=$perimetr0 - $result0;
$result2=$result1 / 2;
print "[+] El largo del rectangulo es$V $result2 $b ...\n";
exit;
}
if($ancho1 == "X"){          #FORMULA: P=2(BMA) + 2(BME);
print "[*] Calculando su ancho del rectangulo ...\n";
system(sleep 0.8);
$result00=$largo1 * 2;
$result11=$perimetr0 - $result00;
$result22=$result11 / 2;
print "[+] Su Ancho del Rectangulo es$V $result22 $b ...$end \n";
exit;
}
if($perimetr0 == "X"){       #FORMULA: P=2(BMA) + 2(BME);
print "[*] Calculando el perimetro del rectangulo ...\n";
system(sleep 0.4);
$rslt=$largo1 * 2;
$rslt1=$ancho1 * 2;
$rsltend=$rslt + $rslt1;
print "[+] El Perimetro del rectangulo es$V $rsltend $b ...\n";
exit;
}
}
if ($OP_FIGURAS == 3){
print "$g \n";
print "Digame el perimetro del triangulo equilatero(X): ";
$perimetrote=<STDIN>;
print "Digame el lado del triangulo equilatero(X): ";
$ladote=<STDIN>;
print "$b \n";
if ($perimetrote == "X"){        #FORMULA: P=l^3
print "[*]̂ Calculando el perimetro del triangulo equilatero ...\n";
system(sleep 0.8);
$result00=$ladote * 3;
print "[+] El Perimetro del triangulo equilatero es$V $result00 $b ...$end \n";
exit;
}
if ($ladote = "X"){        #FORMULA: P=l^3
print "[*] Calculando el lado del triangulo equilatero ... \n";
system(sleep 0.9);
$result092=$perimetrote / 3;
print "[+] El Lado del triangulo equilatero es$V $result092 $b ...$end \n";
exit;
}
}
#FALTA TERMINAF
}

if($op_menu == 9){
	print "$c $end \n";
	print "FIGURAS:
1 --> Cuadrado
2 --> Rectangulo
3 --> Triangulo
4 --> Triangulo Equilatero
5 --> Trapecio
6 --> Paralelogramo
7 --> Circulo
8 --> Longitud de la circunferencia
9 --> Corona Circular
10 --> Rombo
11 --> Sector circular
12 --> Poligono regular\n";
print "$a $end \n";
print "Eliga su opcion: ";
$OP_AREA=<STDIN>;
if($OP_AREA == 1){
	print "$a $end \n";
	print "Digame cuanto mide su lado(X): ";
	$LADO=<STDIN>;
	print "Digame su area(X): ";
	$AREA=<STDIN>;
	if($AREA == "X"){
	print "$b $end \n";
	print "[*] Calculando el area del cuadrado ...\n";
	system(sleep 0.8);
	$RESPUESTA=$LADO * $LADO;
	print "[+] El area de un cuadrado es$V $RESPUESTA $b ...$end \n";
	exit;
}
	if($LADO == "X"){
	print "$b $end \n";
	print "[*] Calculando El lado del cuadrado ...\n";
	system(sleep 0.8);
	$RESPUESTAAREA=sqrt($AREA);
	print "[+] El lado del cuadrado es$V $RESPUESTAAREA $b ...$end \n";
	exit;
	}
}
	
if($OP_AREA == 2){
	print "$a $end \n";
	print "Digame su base del rectangulo(X): ";
	$BASE2=<STDIN>;
	print "Digame su Altura del rectangulo(X): ";
	$ALTURA2=<STDIN>;
	print "Digame su Area del rectangulo(X): ";
	$AREA2=<STDIN>;
	if($AREA2 == "X"){
	print "$b $end \n";
	print "[*] Calculando el Area del rectangulo ...\n";
	os.system(sleep 0.9);
	$RESPUESTA2=$BASE2 * $ALTURA2;
	print "[+] El Area de un rectangulo es$V $RESPUESTA2 $b ...$end \n";
	exit;
}
	if($BASE2 == "X"){
	print "$b $end \n";
	print "[*] Calculando La Base del Rectangulo ...\n";
	os.system(sleep 0.8);
	$RESPUESTA2=$AREA2 / $ALTURA2;
	print "[+] La Base del rectangulo es$V $RESPUESTA2 $b ...$end \n";
	exit;
}
	if($ALTURA2 == "X"){
	print "$b $end \n";
	print "[*] Calculando altura de un Rectangulo ...\n";
	os.system(sleep 0.9);
	$RESPUESTA2=$AREA2 / $BASE2;
	print "[+] La Altura de un rectangulo es$V $RESPUESTA2 $b ...$end \n";
	exit;
}
}

if($OP_AREA == 3){
	print "$a $end \n";
	print "Digame la base del triangulo(X): ";
	$BASE3=<STDIN>;
	print "Digame la Altura del triangulo(X): ";
	$ALTURA3=<STDIN>;
	print "Digame la Area del triangulo(X): ";
	$AREA3=<STDIN>;
	if($BASE3 == "X"){
	print "$b $end \n";
	print "[*] Calculando la base del triangulo ...\n";
	os.system(sleep 0.8);
	$RESPUESTA3=$AREA3 * 2;
	$RESPUESTA3=$RESPUESTA3 / $ALTURA3;
	print "[+] La Base del triangulo es$V $RESPUESTA3 $b ...$end \n";
	exit;
	
}
	if($ALTURA3 == "X"){
	print "$b $end \n";
	print "[*] Calculando La Altura del triangulo ...\n";
	system(sleep 0.7);
	$RESPUESTA3=$AREA3 * 2;
	$RESPUESTA3=$RESPUESTA3 / $BASE3;
	print "[+] La Altura del triangulo es$V $RESPUESTA3 $b ...$end\n";
	exit;
}
	if($AREA3 == "X"){
	print "$b $end \n";
	print "[*] Calculando el area del triangulo ...\n";
	system(sleep 0.8);
	$RESPUESTA3=$BASE3 * $ALTURA3;
	$RESPUESTA3=$RESPUESTA3 / 2;
	print "[+] El Area del triangulo es$V $RESPUESTA3 $b ...$end \n";
	exit;
}
}

if($OP_AREA == 4){
	print "$a $end \n";
	print "Digame su Lado del triangulo equilatero(X): ";
	$LADO4=<STDIN>;
	print "Digame su Area del triangulo equilatero(X): ";
	$AREA4=<STDIN>;
	if($LADO4 == "X"){
	print "$b $end \n";
	print "[*]̂ Calculando El Lado del triangulo equilatero ...\n";
	system(sleep 0.9);
	$RESPUESTA4=$AREA4 * 4;
	$RESPUESTA4=sqrt($RESPUESTA4);
	print "[+] El Lado del Triangulo Equilatero es$V $RESPUESTA4 $b ...$end\n";
	exit;
}
	if($AREA4 == "X"){
	print "$b $end \n";
	print "[*] Calculando el Area del triangulo equilatero ...\n";
	system(sleep 0.8);
	$RESPUESTA4=$LADO4 * $LADO4;
	$RESPUESTA4=$RESPUESTA4 / 4;
	$RESPUESTA4="$RESPUESTA4√3";
	print "[+] El Area del triangulo equilatero es$V $RESPUESTA4 $b ...$end \n";
	exit;
}
}

if($OP_AREA == 5){
	print "$a $end \n";
	print "Digame su Base Mayor del trapecio(X): ";
	$BASEMAYOR5=<STDIN>;
	print "Digame su Base Menor del trapecio(X): ";
	$BASEMENOR5=<STDIN>;
	print "Digame su Altura del trapecio(X): ";
	$ALTURA5=<STDIN>;
	print "Digame Area del trapecio(X): ";
	$AREA5=<STDIN>;
	if($BASEMAYOR5 == "X"){
	print "$b $end \n";
	print "[*] Calculando La Base Mayor del trapecio ...\n";
	system(sleep 0.9);
	$RESPUESTA5=$BASEMENOR5 * $ALTURA5;
	$RESPUESTA51=$AREA5 * 2;
	$RESPUESTA5=$RESPUESTA51 - $RESPUESTA5;
	$RESPUESTA5=$RESPUESTA5 / $ALTURA5;
	print "[+] La Base Mayor del trapecio es$V $RESPUESTA5 $b ...$end\n";
	exit;
}
	if($BASEMENOR5 == "X"){
	print "$b $end \n";
	print "[*] Calculando La Base Menor del trapecio ...\n";
	system(sleep 0.8);
	$RESPUESTA5=$BASEMAYOR5 * $ALTURA5;
	$RESPUESTA51=$AREA5 * 2;
	$RESPUESTA5=$RESPUESTA51 - $RESPUESTA5;
	$RESPUESTA5=$RESPUESTA5 / $ALTURA5;
	print "[+] La Base Menor del trapecio es$V $RESPUESTA5 $b ...$end \n";
	exit;
}
	if($ALTURA5 == "X"){
	print "$b $end \n";
	print "[*] Calculando la Altura del trapecio ...\n";
	$RESPUESTA5=$BASEMAYOR5 + $BASEMENOR5;
	$RESPUESTA5=$RESPUESTA5 / 2;
	$RESPUESTA5=$AREA5 / $RESPUESTA5;
	print "[+] La Altura del trapecio es$V $RESPUESTA5 $b ...$end \n";
	exit;
}
	if($AREA5 == "X"){
	print "$b $end \n";
	print "[*] Calculando Area del trapecio ...\n";
	$RESPUESTA5=$BASEMAYOR5 + $BASEMENOR5;
	$RESPUESTA5=$RESPUESTA5 / 2;
	$RESPUESTA5=$RESPUESTA5 * $ALTURA5;
	print "[+] El area del trapecio es$V $RESPUESTA5 $b ...$end \n";
	exit;
}
}
if($OP_AREA == 6){
	print "$a $end \n";
	print "Digame la Base del Paralelogramo(X): ";
	$BASE6=<STDIN>;
	print "Digame la Altura del Paralelogramo(X): ";
	$ALTURA6=<STDIN>;
	print "Digame el Area del Paralelogramo(X): ";
	$AREA6=<STDIN>;
	if($BASE6 == "X"){
	print "$b $end \n";
	print "[*] Calculando la base del paralelogramo ...\n";
	$RESPUESTA6=$AREA6 / $ALTURA6;
	print "[+] La base del paralelogramo es$V $RESPUESTA6 $b ...$end\n";
	exit;
}
	if($ALTURA6 == "X"){
	print "$b $end \n";
	print "[*] Calculando la altura del paralelogramo ...\n";
	$RESPUESTA6=$AREA6 / $BASE6;
	print "[+] La Altura del paralelogramo es$V $RESPUESTA6 $b ...$end \n";
	exit;
}
	if($AREA6 == "X"){
	print "$b $end \n";
	print "[*] Calculando el area de un paralelogramo ...\n";
	sleep 0.8;
	$RESPUESTA6=$BASE6 * $ALTURA6;
	print "[+] La Area del paralelogramo es$V $RESPUESTA6 $b ...$end\n";
	exit;
}
}
if($OP_AREA == 7){
	print "$a $end \n";
	print "Digame su radio del circulo(X): ";
	$RADIO7=<STDIN>;
	print "Digame su Area del circulo(X): ";
	$AREA7=<STDIN>;
	if($RADIO7 == "X"){
	print "$b $end \n";
	print "[*] Calculando su radio del circulo ...\n";
	os.system(sleep 0.9);
	$RESPUESTA7=sqrt($AREA7);
	print "[+] El Radio del circulo es$V $RESPUESTA7 $b ...$end\n";
	exit;
}
	if($AREA7 == "X"){
	print "$b $end \n";
	print "[*] Calculando el area del circulo ...\n";
	os.system(sleep 0.9);
	$RESPUESTA7=$RADIO7 * $RADIO7;
	$RESPUESTA7="$RESPUESTA7π";
	print "[+] El Area del circulo es$V $RESPUESTA7 $b ...$end \n";
	exit;
}
}

if($OP_AREA == 8){
print "$a \n";
print "Digame su radio(X): ";
$RADIO8=<STDIN>;
print "Digame su area(X): ";
$AREA8=<STDIN>;
print "$b\n";
if($RADIO8 == "X"){
print "[*] Calculando el radio de la longitud de la circunferencia ...\n";
system(sleep 0.8);
$RESULT8=$AREA8 - 2;
print "[+] El radio es$V $RESULT8 $b ...$end \n";
exit;
}
if ($AREA8 == "X"){
print "[*] Calculando el area de la longitud de la circunferencia ...\n";
$RESULT8=$RADIO8 * 2;
print "[+] El area de la longitud de la circunferencia es$V $RESULT8π $b ...$end \n";
exit;
}
}
	
if($OP_AREA == 9){
print "$r \n";
print "Digame el Radio Mayor(X): ";
$RADIOMAYOR9=<STDIN>;
print "Digame el Radio Menor(X): ";
$RADIOMENOR9=<STDIN>;
print "Digame el area de la corona circular(X): ";
$AREA9=<STDIN>;
print "$b \n";
if ($RADIOMAYOR9 == "X"){
print "[*] Calculando Radio Mayor ...\n";
$RESULT9=$RADIOMENOR9**2;
$RESULT91=$AREA9 + $RESULT9;
$RESULT9=sqrt($RESULT91);
print "[+] El Radio mayor de la corona circular es$V $RESULT9 $b ...$end \n";
exit;
}
if ($RADIOMENOR9 == "X"){
print "[*] Calculando Radio Menor ...\n";
$RESULT9=$RADIOMAYOR9**2;
$RESULT91=$AREA9 - $RESULT9;
$RESULT9=sqrt($RESULT91);
print "[+] El Radio menor de la corona circular es$V $RESULT9 $b ...$end \n";
exit;
}
if ($AREA9 == "X"){
print "[*] Calculando Area de la corona circula ...\n";
system "sleep 0.5";
$RESULT9=$RADIOMAYOR9**2 - $RADIOMENOR9**2;
print "[+] El Area de la corona circula es$V $RESULT9π $b ...$end \n";
exit;
}
}

if($OP_AREA == 10){
print "$c \n";
print "Digame la Diagonal Mayor(X): ";
$DIAGONALMAYOR10=<STDIN>;
print "Digame la Diagonal Menor(X): ";
$DIAGONALMENOR10=<STDIN>;
print "Digame su area del rombo(X): ";
$AREA10=<STDIN>;
print "$b \n";
if($DIAGONALMAYOR10 == "X"){
print "[*] Calculando la diagonal mayor ...\n";
$RESULT10=$AREA10 * 2;
$RESULT10=$RESULT10 / $DIAGONALMENOR10;
print "[+] La Diagonal mayor es$V $RESULT10 $b ...$end\n";
exit;
}
if($DIAGONALMENOR10 == "X"){
print "[*] Calculando Diagonal menor ...\n";
$RESULT10=$AREA10 * 2;
$RESULT10=$RESULT10 / $DIAGONALMAYOR10;
print "[+] La Diagonal menor es$V $RESULT10 $b ...$end \n";
exit;
}
if($AREA10 == "X"){
print "[*] Calulando El Area del rombo ...\n";
$RESULT10=$DIAGONALMAYOR10 * $DIAGONALMENOR10;
$RESULT10=$RESULT10 / 2;
print "[+] El Area del rombo es$V $RESULT10 $b ...$end \n";
exit;
}
}
if($OP_AREA == 11){
print "$r \n";
print "Digame el radio(X): ";
$radio11=<STDIN>;
print "Digame los grados del sector(X): ";
$gradoss=<STDIN>;
print "Digame el area del sector circular(X): ";
$sectorcircular=<STDIN>;
print "$b \n";
if($radio11 == "X"){
print "[*] Calculando el radio del sector circular ...\n";
$RESULT11=$sectorcircular * 360;
$RESULT11=$RESULT11 / $gradoss;
$RESULT11=sqrt($RESULT11);
print "[+] El radio del sector circular es$V $RESULT11 $b ...$end\n";
exit;
}
if($gradoss == "X"){
print "[*] Calculando los grados del sector circular ...\n";
$RESULT11=$sectorcircular * 360;
$Re=$radio11**2;
$RESULT11=$RESULT11 / $Re;
print "[+] Los grados del sector circular es$V $RESULT11 $b ...$end\n";
exit;
}
if($sectorcircular == "X"){
print "[*] Calculando el area del sector circular ...\n";
$RESULT11=$radio11**2 * $gradoss;
$RESULT11=$RESULT11 / 360;
print "[+] El area del sector circular es$V $RESULT11π $b ...$end\n";
exit;
}
}
if ($OP_AREA == 12){
print "$v \n";
print "Digame Su Perimetro(X): ";
$perimetropoligono=<STDIN>;
print "Digame Su apotema(X): ";
$apotema=<STDIN>;
print "Digame Su area(X): ";
$areapoligono=<STDIN>;
print "$b \n";
if($perimetropoligono == "X"){
print "[*] Calculando el perimetro del poligono ...\n";
$Res=$areapoligono * 2;
$Res=$Res / $apotema;
print "[+] El perimetro del poligono regular es$V $Res $b ...$end \n";
exit;
}
if($apotema == "X"){
print "[*] Calculando la apotema del poligono regular ...\n";
$RESD=$areapoligono * 2;
$RESD=$RESD / $perimetropoligono;
print "[+] La apotema del poligono regular es$V $RESD $b ...$end\n";
exit;
}
if($areapoligono == "X"){
print "[*] Calculando el area del poligono regular ...\n";
$Resul=$perimetropoligono * $apotema;
$Resul=$Resul / 2;
print "[+] El area del poligono regular es$V $Resul $b ...$end\n";
exit;
}
}

} #FINAL;

if ($op_menu == 10){
print "$A \n";
print "Digame la hipotenusa(X): ";
$hipotenusa10=<STDIN>;
print "Digame el cateto 1(X): ";
$cateto110=<STDIN>;
print "Digame el cateto 2(X): ";
$cateto210=<STDIN>;
print "$b \n";

if($hipotenusa10 == "X"){
print "[*] Calculando la hipotenusa ...\n";
sleep 0.4;
$RESUL10=$cateto110**2 + $cateto210**2;
$RESUL10=sqrt($RESUL10);
print "[+] La Hipotenusa es$V $RESUL10 $b ...$end\n";
exit;
}
if($cateto110 == "X"){
print "[*] Calculando Cateto1 ...\n";
$RESULT110=$hipotenusa10**2;
$RESULT111=$cateto210**2;
$RESULT112=$RESULT110 - $RESULT111;
$RESULTEND=sqrt($RESULT112);
print "[+] El Cateto1 es$V $RESULTEND $b ...$end\n";
exit;
}
if($cateto210 == "X"){
print "[*] Calculando Cateto2 ...\n";
$RESULT110=$hipotenusa10**2 - $cateto110**2;
$RESULTEND=sqrt($RESULT110);
print "[+] El Cateto2 es$V $RESULTEND $b ...$end\n";
exit;
}
}