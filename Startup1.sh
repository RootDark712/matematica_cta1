#!/usr/bin/sh o
#!/usr/bin/env sh o
#!/system/bin/sh o
#!/system/bin/env sh

#-----------------------INFO--------------------#

#NAME: startup.sh                               #
#Function: Modulo de inicio de matematica.pl    #
#Programador: MR_ROBOT (Kevin Huanca Fernandez) #
#Grupo: FSOCIETY                                #

#-----------------------INFO--------------------#

#[ALERT] ESTE ESCRIPT SE MANTENDRA DE FUNCIONANDO HASTA QUE SE CREE UNA FUNCION EN EL SCRIPT..
# STARTING CODED ...

MENU ( ){
perl matematica_cta.pl
data=$(cat opc.conf);
rm opc.conf;
trap ctrl_c INT
ctrl_c(){
echo "";
echo "[*] Gracias Por Usar Nuestro Proyecto ...";
echo "[*] Create By Fsociety ...";
echo "[+] Saliendo ...";
exit;
}
if [ "$data" = "99" ];then
echo "";
exit;
else
MENU
fi
}
MENU
